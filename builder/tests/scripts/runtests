#!/bin/sh

# by default, use the java executable on the path
export JAVA_HOME=/opt/sun-java2-5.0
vm=java

#this value must be set when using rsh to execute this script, otherwise the script will execute from the user's home directory
dir=.

# operating system, windowing system and architecture variables
os=
ws=
arch=

# list of tests (targets) to execute in test.xml
tests=

# default target to run in org.eclipse.emf.releng.build/tests/scripts/test.xml
antTestTarget=all

# default value to determine if eclipse should be reinstalled between running of tests
installmode="clean"

# name of a property file to pass to Ant
properties=

# message printed to console
usage="usage: $0 -os <osType> -ws <windowingSystemType> -arch <architecture> [-noclean] [<test target>][-properties <path>]"

Xflags="";
Dflags="";

echo "[runtests] runtests started on: `date +%Y%m%d\ %H\:%M\:%S`";

# process command line arguments
echo "[runtests] Got these commandline args:"
while [ $# -gt 0 ]
do
	case "$1" in
		-project) project="$2"; echo "  "$1 $2; shift;;
		-dir) dir="$2"; echo "  "$1 $2; shift;;
		-os) os="$2"; echo "  "$1 $2; shift;;
		-ws) ws="$2"; echo "  "$1 $2; shift;;
		-arch) arch="$2"; echo "  "$1 $2; shift;;
		-noclean) installmode="noclean" echo "  "$1;;
		-properties) properties="-propertyfile $2"; echo "  "$1 $2; shift;;
		-javaHome) javaHome="$2"; echo "  "$1 $2; shift;;
		-cp) cp="$2"; echo "  "$1 $2; shift;;
		-antTestTarget) antTestTarget="$2"; echo "  "$1 $2; shift;;
		-X*) Xflags=$Xflags\ $1; echo "  "$1;;
		-D*) Dflags=$Dflags\ $1; echo "  "$1;;
	esac
	shift
done

if [ "x$javaHome" != "x" ]; then export JAVA_HOME=$javaHome; fi

# for *nix systems, os, ws and arch values must be specified
if [ "x$os" = "x" ]
then
	echo >&2 "$usage"
	exit 1
fi

if [ "x$ws" = "x" ]
then
	echo >&2 "$usage"
	exit 1
fi

if [ "x$arch" = "x" ]
then
	echo >&2 "$usage"
	exit 1
fi

execCmd () 
{
	echo ""; echo "[runtests] [`date +%H\:%M\:%S`]"; 
	echo "  $1" | perl -pe "s/ -/\n  -/g"
	$1;
}

#necessary when invoking this script through rsh
cd $dir
workspaceDir="$dir/eclipse/workspace"

# verify os, ws and arch values passed in are valid before running tests
if [ "$os-$ws-$arch" = "linux-motif-x86" ] || [ "$os-$ws-$arch" = "linux-gtk-x86" ] || [ "$os-$ws-$arch" = "solaris-motif-sparc" ] || [ "$os-$ws-$arch" = "aix-motif-sparc" ] || [ "$os-$ws-$arch" = "hpux-motif-ppc" ] || [ "$os-$ws-$arch" = "qnx-photon-x86" ]
then
	# Replace the boot eclipse (The eclipse used to run the main test.xml, this will start another eclipse later)
	if [ -d $dir/eclipse ] ; then
		rm -rf $dir/eclipse
	fi
	if [ -d $workspaceDir ] ; then
		rm -rf $dir/workspace
	fi

	echo "[runtests] Currently in `pwd`:"
	# need conditional processing here: M3.0.2 = zip, I3.1.0 = tar.gz
	sdks=`find $dir -name "eclipse-SDK-*"`
	# get extension from file(s)
	for sdk in $sdks; do
		sdk="eclipse"${sdk##*eclipse}; # trim up to eclipse (substring notation)
		echo -n "[runtests] Eclipse SDK $sdk is a";
		ext=${sdk%%\.zip}; # trim off .zip (substring notation)
		if [ "$ext" != "$sdk" ]; then # it's a zip
			echo " zip. Unpacking...";
			unzip -qq -o $sdk
		else
			ext=${sdk%%\.tar\.gz}; # trim off .tar.gz (substring notation)
			if [ "$ext" != "$sdk" ]; then # it's a tar.gz
				echo " tar.gz. Unpacking...";
				tar -xzf $sdk
			else
				ext=${sdk%%\.tar\.Z}; # trim off .tar.Z (substring notation)
				if [ "$ext" != "$sdk" ]; then # it's a tar.Z
					echo " tar.Z. Unpacking...";
					tar -xZf $sdk
				else
					echo "n UNKNOWN file type. Script aborting!";
					exit 2
				fi
			fi
		fi
	done

	J2SE15flags="";
	# TODO: if a 1.5 JDK and want source/target = 1.5, leave these in
	# TODO: if source/target = 1.4, remove these!
	if [ ${JAVA_HOME##*1.5*}"" = "" -o ${JAVA_HOME##*15*}"" = "" -o ${JAVA_HOME##*5.0*}"" = "" -o ${JAVA_HOME##*50*}"" = "" ]; then
		# set J2SE-1.5 properties (-Dflags)
		bootclasspath="."`find $JAVA_HOME/jre/lib -name "*.jar" -printf ":%p"`;
		J2SE15flags=$J2SE15flags" -DJ2SE-1.5=$bootclasspath"
		J2SE15flags=$J2SE15flags" -DbundleBootClasspath=$bootclasspath"
		J2SE15flags=$J2SE15flags" -DjavacSource=1.5"
		J2SE15flags=$J2SE15flags" -DjavacTarget=1.5"
		J2SE15flags=$J2SE15flags" -DbundleJavacSource=1.5"
		J2SE15flags=$J2SE15flags" -DbundleJavacTarget=1.5"
	fi
	
	# default classpath - can be overridden by relengbuildgtk.sh
	# up to Eclipse 3.3M4, use eclipse/startup.jar
	# after Eclipse 3.3M4, use eclipse/plugins/org.eclipse.equinox.launcher_*.jar
	cp=`find eclipse/ -name "org.eclipse.equinox.launcher_*.jar" | sort | head -1`
	
	# run tests
	echo "[runtests] [`date +%H\:%M\:%S`] Launching Eclipse (installmode = $installmode with -enableassertions turned on) ..."
	execCmd "$JAVA_HOME/bin/java $Xflags -enableassertions -cp $cp org.eclipse.equinox.launcher.Main -ws $ws -os $os -arch $arch \
	-application org.eclipse.ant.core.antRunner -data $workspaceDir -file test.xml $antTestTarget \
	$Dflags -Dws=$ws -Dos=$os -Darch=$arch -D$installmode=true $J2SE15flags \
	$properties -logger org.apache.tools.ant.DefaultLogger";
	echo "[runtests] [`date +%H\:%M\:%S`] Eclipse test run completed. "
	
	# display message to user if os, ws and arch are invalid
	else
		echo "The os, ws and arch values are either invalid or are an invalid combination"
	
	exit 1
fi
